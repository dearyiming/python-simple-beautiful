#!/usr/bin/env python
# coding: utf-8

import time
from string import Template
import datetime

on_duty_list = ['孙悟空', '沙和尚', '猪八戒', '白龙马']
on_duty_period_in_days = 7 # day
notify_days = [1, 5]
on_duty_first_person = '孙悟空'
on_duty_first_day = '2022-03-24 10:00:00'
oneDayTs = 24 * 60 * 60

notify_template = Template(r"""
【===========报警值班提醒========】
【本周值班人】${on_duty_person_this_week}
【值班时间】 ${on_duty_range}
【下周值班人】${on_duty_person_next_week}
""")

def SendMessage(message='test'):
    print message

def PairListToMessage(pair_list):
    conent_arr = []
    for k, v in pair_list:
        conent_arr.append(' '.join(['【' + k + '】', v]))
    return '\n'.join(conent_arr)

def UnixTimeFromDate(time_str, format='%Y-%m-%d %H:%M:%S'):
    # 先转换为时间数组
    timeArray = time.strptime(time_str, "%Y-%m-%d %H:%M:%S")
    # 转换为时间戳
    timeStamp = int(time.mktime(timeArray))
    return timeStamp

def DatetimeStr(timeTs, format='%Y-%m-%d %H:%M:%S'):
    return time.strftime(format, time.localtime(timeTs))

def WhoIsOnDuty(timeTs = int(time.time())):
    ref_timeTs = UnixTimeFromDate(on_duty_first_day)
    deltaTs = timeTs - ref_timeTs
    deltaDay = deltaTs / oneDayTs
    index = deltaDay / on_duty_period_in_days % len(on_duty_list)
    day_index = deltaDay % on_duty_period_in_days
    ts = time.localtime(timeTs)
    start_day = str(datetime.date(ts.tm_year, ts.tm_mon, ts.tm_mday) + datetime.timedelta(days = -day_index))
    end_day = str(datetime.date(ts.tm_year, ts.tm_mon, ts.tm_mday) + datetime.timedelta(days = - day_index + on_duty_period_in_days - 1))
    return on_duty_list[index], day_index + 1, start_day, end_day, on_duty_list[(index+1)%len(on_duty_list)]


# 周四、每周一提醒本周值班人员，下周值班人员
notify_counter = 0
on_duty_person_this_week = ''
while True:
    timeTs = int(time.time())
    timeStr = DatetimeStr(timeTs)
    on_duty_person, day, start_day, end_day, on_duty_person_next = WhoIsOnDuty(timeTs)
    if on_duty_person != on_duty_person_this_week:
        on_duty_person_this_week = on_duty_person
        notify_counter = 0
        SendMessage(notify_template.safe_substitute({
            'on_duty_person_this_week': on_duty_person,
            'on_duty_person_next_week': on_duty_person_next,
            'on_duty_range': '~'.join([start_day, end_day]),
        }))
        notify_counter += 1
    else:
        if notify_counter < 2 and day in notify_days:
            SendMessage(notify_template.safe_substitute({
                'on_duty_person_this_week': on_duty_person,
                'on_duty_person_next_week': on_duty_person_next,
                'on_duty_range': '~'.join([start_day, end_day]),
            }))
            notify_counter += 1
    time.sleep(1)