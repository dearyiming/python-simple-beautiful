# 用Markdown写邮件，用Python发邮件


![picture 1](https://s2.loli.net/2022/03/27/RAeWGwsHOCvkL3m.png)  

平时工作过程中难免要使用邮件，现有的邮件客户端在编辑体验上都不怎么友好，在调整格式时尤其痛苦。以我的有限的人生经验来看，所见即所得的编辑软件往往不如纯文本编辑体验流畅。近些年来，Markdown逐渐成为写作的利器，甚至现在有些出版社也已经接收Markdown手稿。
那么，我们能否使用Markdown来写邮件呢，然后写个Python小脚本去发送邮件呢？

邮件通信的内容使用MIME（Multipurpose Internet Mail Extension)编码，MIME之于邮件客户端类似于html之于浏览器的关系。MIME支持邮件承载多媒体内容，可以把MIME理解为受限的html/css，但是MIME不支持js脚本(安全原因)。

整个程序的工作原理如下图：
![picture 2](https://s2.loli.net/2022/03/27/QV9f5NpgixU71Xq.png)  


## 在自己喜欢的Markdown编辑器下编辑markdown文档/邮件
记得在markdown文档头部添加formatter
```md
From: foo <foo@qq.com>
To: bar <bar@qq.com>
Subject: 测试Markdown邮件
---
```
## 配置个人邮箱账号、密码(切记不要在公共电脑存放个人密码，也不要上传到公网)
可以存放到`~/.markdown-to-email.json`
```json
{
    "username": "your_account", 
    "smtp": "smtp.qq.com:587", 
    "password": "your password/authorization code" 
}
```
> QQ邮箱服务在使用第三方客户端登录时，需要申请授权码。详见：[什么是授权码，它又是如何设置？](https://service.mail.qq.com/cgi-bin/help?subtype=1&&id=28&&no=1001256%27)

## 执行如下的Python代码
完整示例，参考git仓库: https://gitee.com/dearyiming/python-simple-beautiful/tree/master/src/02-email

### 本地预览
```bash
python markdown_email.py -p --md <your markdown file>
```
注意：mac电脑使用`open`命令可以直接调用邮箱APP打开预览，其他操作系统可以使用邮箱APP导入邮件(eml文件)去预览
### 发送邮件
```bash
python markdown_email.py -s --md <your markdown file>
```

```python
#!/usr/bin/env python

'''
Send an multipart email with HTML and plain text alternatives. The message
should be constructed as a plain-text file of the following format:
    
    From: Your Name <your@email.com>
    To: Recipient One <recipient@to.com>
    Subject: Your subject line
    ---
    Markdown content here
The script accepts content from stdin and, by default, prints the raw
generated email content to stdout.
Preview your message on OS X by invoking the script with `-p` or 
`--preview`, and it will open in your default mail client.
To send the message, invoke with `-s` or `--send`. You must have a
JSON file in your home directory named `.markdown-to-email.json`
with the following keys:
    {
        "username": "smtp-username", 
        "smtp": "smtp.gmail.com:587", 
        "password": "your-password" 
    }
Enjoy!
'''


import os
import sys
import json
import argparse
import smtplib
import subprocess

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pygments
import markdown2 as markdown

# define arguments
parser = argparse.ArgumentParser(description='Format and send markdown-based emails.',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=__doc__)
parser.add_argument('-p', '--preview', action='store_true', 
                    help='Preview the email in Apple Mail.')
parser.add_argument('-s', '--send', action='store_true', 
                    help='Send the email using your configuration.')

parser.add_argument('--md', dest='markdown', type=str, nargs=1, required=True)
args = parser.parse_args()
print(args)
# read in raw message content
raw_content = open(args.markdown[0], 'r').read()

# split out the headers from the markdown message body
header_content, markdown_content = raw_content.split('---', 1)

# render the markdown into HTML 
css = subprocess.check_output(['pygmentize', '-S', 'default', '-f', 'html'])
markdown_content = markdown_content.strip()
html_content = markdown.markdown(markdown_content)
html_content = ''.join([
    '<style type="text/css">',
    str(css),
    '</style>',
    html_content
])

# create a multipart email message
message = MIMEMultipart('alternative')

# parse the headers
headers = {}
for line in header_content.strip().split('\n'):
    if not line.strip(): continue
    key, value = line.split(':', 1)
    headers[key.strip()] = value.strip()

# set the headers
message['To'] = headers.get('To', '')
message['From'] = headers.get('From', '')
message['Subject'] = headers.get('Subject', 'No subject')

# attach the message parts
message.attach(MIMEText(markdown_content, 'plain')) # 如果邮件客户端不支持html，显示Markdown源码
message.attach(MIMEText(html_content, 'html')) # 如果邮件客户端支持html，显示Markdown渲染后的html

if args.send:
    to = message['To'].split(', ')
    
    with open(os.path.expanduser('~/.markdown-to-email.json'), 'rb') as f:
        config = json.loads(f.read())
        server = smtplib.SMTP(config['smtp'])
        server.starttls()
        server.login(config['username'], config['password'])
        server.sendmail(message['From'], to, message.as_string())
        server.quit()
elif args.preview:
    email_dump = '/tmp/preview-with-css.eml'
    open(email_dump, 'w').write(message.as_string())
    os.system('open -a Mail {}'.format(email_dump))
else:
    print(message.as_string())
```

### 邮件效果
![picture 3](https://s2.loli.net/2022/03/27/VHqnIzxoygTMtfu.png)  


## 参考信息
1. [理解邮件传输协议（SMTP、POP3、IMAP、MIME） ](https://www.cnblogs.com/diegodu/p/4097202.html)
2. [cleverdevil/markdown-to-email](https://gist.github.com/cleverdevil/8250082)